package com.aircountr.android.prefrences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Created by gaurav on 5/1/2016.
 */
public class AppPreferences {
    private static SharedPreferences pref;
    private static Editor editor;
    private Context _context;
    private int PRIVATE_MODE = 0;
    private static final String PREFER_NAME = "AircountrPrefs";
    public static String KEY_ACCESS_TOKEN = "token";
    public static String KEY_MERCHANT_ID = "merchantId";
    public static String KEY_BUSINESS_NAME = "businessname";
    public static String KEY_EMAIL_ID = "email";
    public static String KEY_PHONE_NO = "mobile";
    public static String KEY_OPERATIONAL_HOUR_FROM = "openTime";
    public static String KEY_OPERATIONAL_HOUR_TO = "closeTime";
    public static String KEY_NO_OF_EMPLOYEE = "noofemployees";
    public static String KEY_PASSWORD = "password";
    public static String KEY_AUTO = "auto/manual";
    public static String KEY_REF = "referralCode";
    public static String KEY_REG = "regtoken";

    public AppPreferences(Context context) {
        this._context = context;
        this.pref = this._context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.commit();
    }

    public static boolean getAutoOrManual(Context _context){
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME,_context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(KEY_AUTO,true);
    }

    public static void setAutOrManual(Context _context,boolean keyAuto){
        Editor editor=_context.getSharedPreferences(PREFER_NAME,_context.MODE_PRIVATE).edit();
        editor.putBoolean(KEY_AUTO,keyAuto);
        editor.commit();
    }

    public static String getPassword(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_PASSWORD, "");
    }

    public static void setPassword(Context _context,String keyPassword) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_PASSWORD, keyPassword);
        editor.commit();
    }

    public static String getAccessToken(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_ACCESS_TOKEN, "");
    }

    public static void setAccessToken(Context _context, String accessToken) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_ACCESS_TOKEN, accessToken);
        editor.commit();
    }

    public static String getMerchantId(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_MERCHANT_ID, "");
    }

    public static void setMerchantId(Context _context, String merchantId) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_MERCHANT_ID, merchantId);
        editor.commit();
    }

    public static String getBusinessName(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_BUSINESS_NAME, "");
    }

    public static void setBusinessName(Context _context, String businessName) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_BUSINESS_NAME, businessName);
        editor.commit();
    }
    public static String getEmailId(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_EMAIL_ID, "");
    }

    public static void setEmailId(Context _context, String emailId) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_EMAIL_ID, emailId);
        editor.commit();
    }
    public static String getPhoneNo(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_PHONE_NO, "");
    }

    public static void setPhoneNo(Context _context, String mobile) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_PHONE_NO, mobile);
        editor.commit();
    }
    public static String getOperationalHourFrom(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_OPERATIONAL_HOUR_FROM, "");
    }

    public static void setOperationalHourFrom(Context _context, String op_hourfrom) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_OPERATIONAL_HOUR_FROM, op_hourfrom);
        editor.commit();
    }
    public static String getOperationalHourTo(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_OPERATIONAL_HOUR_TO, "");
    }

    public static void setOperationalHourTo(Context _context, String op_hourTo) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_OPERATIONAL_HOUR_TO, op_hourTo);
        editor.commit();
    }
    public static String getRefferalCode(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_REF, "");
    }
    public static void setRefferalCode(Context _context, String refferal) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_REF, refferal);
        editor.commit();
    }
    public static String getRegestrationToken(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_REG, "");
    }
    public static void setRegestrationToken(Context _context, String refferal) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_REG, refferal);
        editor.commit();
    }
    public static String getNoOfEmployee(Context _context) {
        SharedPreferences sharedPreferences = _context.getSharedPreferences(PREFER_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_NO_OF_EMPLOYEE, "");
    }
    public static void setNoOfEmployee(Context _context, String noofemployee) {
        Editor editor = _context.getSharedPreferences(PREFER_NAME, _context.MODE_PRIVATE).edit();
        editor.putString(KEY_NO_OF_EMPLOYEE, noofemployee);
        editor.commit();
    }
}