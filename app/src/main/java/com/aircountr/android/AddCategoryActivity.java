package com.aircountr.android;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.aircountr.android.adapters.AddCategoryIconsListAdapter;
import com.aircountr.android.constants.CreateUrl;
import com.aircountr.android.constants.UrlConstants;
import com.aircountr.android.model.Category;
import com.aircountr.android.prefrences.AppPreferences;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.splunk.mint.Mint;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by gaurav on 4/26/2016.
 */
public class AddCategoryActivity extends BaseActivity {
    private String TAG = this.getClass().getSimpleName();
    private ImageView iv_backBtn;
    private TextView tv_pageTitle;
    private EditText et_categoryName;
    private TextView tv_errCategoryName;
    private TextView tv_selectCategoryImgText;
    private GridView gv_categoryIconList;
    private TextView tv_saveBtn;
    private AddCategoryIconsListAdapter mAdapter;
    private ArrayList<Category> iconsList;
    private Category rowData;
    private List<String> iconNames = new ArrayList<String>();
    private String iconName = "";
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set the application environment
        Mint.setApplicationEnvironment(Mint.appEnvironmentStaging);
        Mint.initAndStartSession(AddCategoryActivity.this, "058c75a4");

        setContentView(R.layout.activity_add_category);

        iv_backBtn = (ImageView) findViewById(R.id.iv_backBtn);
        tv_pageTitle = (TextView) findViewById(R.id.tv_pageTitle);
        et_categoryName = (EditText) findViewById(R.id.et_categoryName);
        tv_errCategoryName = (TextView) findViewById(R.id.tv_errCategoryName);
        tv_selectCategoryImgText = (TextView) findViewById(R.id.tv_selectCategoryImgText);
        gv_categoryIconList = (GridView) findViewById(R.id.gv_categoryIconList);
        tv_saveBtn = (TextView) findViewById(R.id.tv_saveBtn);

        tv_pageTitle.setTypeface(SEMIBOLD);
        et_categoryName.setTypeface(SEMIBOLD);
        tv_errCategoryName.setTypeface(REGULAR);
        tv_selectCategoryImgText.setTypeface(REGULAR);
        tv_saveBtn.setTypeface(SEMIBOLD);

        iconsList = new ArrayList<>();
        mAdapter = new AddCategoryIconsListAdapter(AddCategoryActivity.this, iconsList);
        gv_categoryIconList.setAdapter(mAdapter);
        if (isNetworkAvailable()) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
            final String url = CreateUrl.getCategoryIconsUrl();

            JsonObjectRequest categoryIcons = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "GetCategoryIcons: " + response.toString());
                    progressDialog.hide();

                    try {
                        JSONArray iconsArray = response.getJSONArray("data");
                        for (int i = 0; i < iconsArray.length(); i++) {
                            iconNames.add((String) iconsArray.get(i));
                        }
                    } catch (JSONException e) {
                        Log.d(TAG, "Error: " + e.getMessage());
                    }

                    for (int i = 0; i < iconNames.size(); i++) {
                        rowData = new Category();
                        rowData.setCategoryURL(UrlConstants.BASE_URL_ICONS + iconNames.get(i));
                        Log.d(TAG, "URLs: " + iconNames.get(i));
                        rowData.setSelected(false);
                        iconsList.add(rowData);
                    }

                    mAdapter.onDataSetChanged(iconsList);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "Error" + error.getMessage());
                    progressDialog.hide();
                }
            });
            // Adding request to request queue
            AircountrApplication.getInstance().addToRequestQueue(categoryIcons);
        }

        gv_categoryIconList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                iconName = iconsList.get(position).getCategoryURL();
                for (int i = 0; i < iconsList.size(); i++) {
                    iconsList.get(i).setSelected(false);
                }
                iconsList.get(position).setSelected(true);
                mAdapter.onDataSetChanged(iconsList);
            }
        });

        tv_saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidations()) {
                    sendAddCategoryRequest(et_categoryName.getText().toString().trim(), iconName);
                }
            }
        });

        iv_backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddCategoryActivity.this.finish();
            }
        });

        et_categoryName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                if (s.length() > 0) {
                    tv_errCategoryName.setVisibility(View.GONE);
                } else {
                    tv_errCategoryName.setVisibility(View.VISIBLE);
                    tv_errCategoryName.setText("Plz enter category name");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private boolean checkValidations() {
        if (et_categoryName.getText().toString().trim().isEmpty()) {
            tv_errCategoryName.setVisibility(View.VISIBLE);
            tv_errCategoryName.setText("Plz enter category name");
            return false;
        } else if (iconName == "") {
            showDialog("Alert", "Plz select suitable category icon", "OK");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hideProgressDialog();
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void sendAddCategoryRequest(String categoryName, String resourceId) {
        if (AppPreferences.getMerchantId(AddCategoryActivity.this).equals("") && AppPreferences.getAccessToken(AddCategoryActivity.this).equals("")) {
            switchActivity(AddCategoryActivity.this, SignInActivity.class);
            AddCategoryActivity.this.finish();
        } else {
            if (isNetworkAvailable()) {
                new AddCategoryAsync().execute(AppPreferences.getAccessToken(AddCategoryActivity.this), categoryName, AppPreferences.getMerchantId(AddCategoryActivity.this), iconName);
            } else {
                showDialog("Error", getResources().getString(R.string.no_internet), "OK");
            }
        }
    }

    private class AddCategoryAsync extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.addCategoriesUrl();
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(),
                    30000);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Authorization", params[0]);
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("name", params[1]);
                jsonObject.put("merchantId", params[2]);
                jsonObject.put("url", params[3]);
                httpPost.setEntity(new ByteArrayEntity(jsonObject.toString().getBytes("UTF8")));

                HttpResponse response = httpClient.execute(httpPost);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                AircountrApplication.getInstance().trackException(e);
                e.printStackTrace();
            } catch (JSONException e) {
                AircountrApplication.getInstance().trackException(e);
                e.printStackTrace();
            }
            return _response;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Plz Wait...", false);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                try {
                    JSONObject response = new JSONObject(result);
                    boolean success = response.getBoolean("success");
                    String message = response.getString("msg");
                    if (success) {
                        AircountrApplication.getInstance().trackEvent("Action","Add Category");
                        displayToast(message);
                        AddCategoryActivity.this.finish();
                    } else {
                        showDialog("Alert", message, "OK");
                    }

                } catch (JSONException e) {
                    AircountrApplication.getInstance().trackException(e);
                    e.printStackTrace();
                }
            }
            hideLoading();
        }
    }
}