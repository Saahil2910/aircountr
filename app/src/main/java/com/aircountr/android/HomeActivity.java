package com.aircountr.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.SwitchCompat;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.aircountr.android.adapters.DrawerListAdapter;
import com.aircountr.android.fragment.HomeFragment;
import com.aircountr.android.fragment.MyInvoicesFragment;
import com.aircountr.android.objects.CategoryListItem;
import com.aircountr.android.prefrences.AppPreferences;
import com.aircountr.android.smoochIo.SmoochIO;
import com.pushbots.push.Pushbots;
import com.splunk.mint.Mint;

import java.util.ArrayList;
import java.util.List;

import io.karim.MaterialTabs;


/**
 * Created by gaurav on 4/26/2016.
 */
public class HomeActivity extends FragmentActivity {
    private String TAG = this.getClass().getSimpleName();
    public FragmentManager fragment_mgr;
    private Fragment mContent;
    public Typeface REGULAR, SEMIBOLD;
    public DrawerLayout drawerLayout;
    private ImageView iv_leftMenu;
    public EditText et_search;
    private ImageView iv_searchBtn;
    private ImageView iv_calenderBtn;
    private LinearLayout ll_leftDrawerLayout;
    private TextView tv_businessName,tv_Auto;
    private ListView lv_drawerList;
    public MaterialDialog processing;
    private DrawerListAdapter mDrawerListAdapter;
    private MaterialTabs tabs;
    public static ViewPager viewPager;
    private SwitchCompat sc_Auto;
    public ArrayList<CategoryListItem> categoryListItems;
    public static boolean isInvoiceListChanged=false;
    private long back = 0;
    private int count = 0,numberOfLoadings=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set the application environment
        Mint.setApplicationEnvironment(Mint.appEnvironmentStaging);
        Mint.initAndStartSession(HomeActivity.this, "058c75a4");

        setContentView(R.layout.activity_home);
        //Initialising notifications as soon as user sign in to homeActivity
        Pushbots.sharedInstance().init(this);
        String var = AppPreferences.getRegestrationToken(HomeActivity.this);
        Log.d("token",var);

        REGULAR = Typeface.createFromAsset(getAssets(), "ProximaNova-Regular.ttf");
        SEMIBOLD = Typeface.createFromAsset(getAssets(), "ProximaNova-Semibold.ttf");

/*        fragment_mgr = getSupportFragmentManager();
        if (savedInstanceState != null) {
            mContent = getSupportFragmentManager()
                    .findFragmentByTag("mContent");
        }

        if (mContent == null)
            mContent = new HomeFragment();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, mContent).commit();
*/
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        iv_leftMenu = (ImageView) findViewById(R.id.iv_leftMenu);
        et_search = (EditText) findViewById(R.id.et_search);
        iv_searchBtn = (ImageView) findViewById(R.id.iv_searchBtn);
        iv_calenderBtn = (ImageView) findViewById(R.id.iv_calenderBtn);
        ll_leftDrawerLayout = (LinearLayout) findViewById(R.id.tv_leftDrawerLayout);
        tv_businessName = (TextView) findViewById(R.id.tv_businessName);
        tv_Auto = (TextView) findViewById(R.id.tv_Auto);
        lv_drawerList = (ListView) findViewById(R.id.lv_drawerList);
        tabs = (MaterialTabs) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        sc_Auto=(SwitchCompat)findViewById(R.id.sc_Auto);
        sc_Auto.setChecked(AppPreferences.getAutoOrManual(getApplicationContext()));

        categoryListItems = ((AircountrApplication)getApplicationContext()).mCategoryListItems;

        ArrayList<String> tabsList = new ArrayList<>();
        tabsList.add("Home");
        tabsList.add("My Invoices");

        ArrayList<String> drawerList = new ArrayList<>();
        drawerList.add(0, "My Vendors");
        //drawerList.add(1, "Wallet");
        drawerList.add(1, "Reports");
        drawerList.add(2, "Refer & Earn");
        drawerList.add(3, "Chat with us");
        drawerList.add(4, "Rate us");
        drawerList.add(5, "Settings");

        mDrawerListAdapter = new DrawerListAdapter(HomeActivity.this, drawerList);
        lv_drawerList.setAdapter(mDrawerListAdapter);

        et_search.setTypeface(REGULAR);
        tv_businessName.setTypeface(SEMIBOLD);
        tv_Auto.setTypeface(SEMIBOLD);
        tv_businessName.setText(AppPreferences.getBusinessName(this));
        drawerLayout.setDrawerListener(new LeftMenuDrawer());

        iv_leftMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(ll_leftDrawerLayout);
            }
        });

        lv_drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (drawerLayout.isDrawerOpen(ll_leftDrawerLayout)) {
                    drawerLayout.closeDrawer(ll_leftDrawerLayout);
                }
                switch (position) {
                    case 0:
                        switchActivity(HomeActivity.this, MyVendorsActivity.class);
                        break;
                    /*case 1:
                        switchActivity(HomeActivity.this, WalletActivity.class);
                        break;*/
                    case 1:
                        switchActivity(HomeActivity.this, ExpenseActivity.class);
                        break;
                    case 2:
                        String ref = AppPreferences.getRefferalCode(HomeActivity.this);
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT,
                                "Download App AirCountr at: https://play.google.com/store/apps/details?id=com.aircountr.android \n Use refferal code " + ref + " to earn credits");
                        sendIntent.setType("text/plain");
                        startActivity(sendIntent);
                        break;
                    case 3:
                        switchActivity(HomeActivity.this, SmoochIO.class);
                        break;
                    case 4:
                        displayToast("Rate us");
                        break;
                    case 5:
                        switchActivity(HomeActivity.this, SettingsActivity.class);
                        finish();
                        break;
                }
            }
        });

        iv_calenderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchActivity(HomeActivity.this, CalendarActivity.class);
            }
        });

        sc_Auto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                AppPreferences.setAutOrManual(getApplicationContext(), isChecked);
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                checkForUpdate(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        setupViewPager(viewPager, tabsList);
    }
    public static void checkForUpdate(int position){
        if (position == 1 && isInvoiceListChanged) {
            isInvoiceListChanged=false;
            MyInvoicesFragment.initialised=false;
            viewPager.getAdapter().notifyDataSetChanged();
        }
    }

    public <T> void switchActivity(Context context, Class<T> startActivity) {

        Intent intent = new Intent(context, startActivity);
        startActivity(intent);
    }

    public void displayToast(int id) {
        Toast.makeText(getApplicationContext(), getResources().getString(id),
                Toast.LENGTH_SHORT).show();
    }

    public void displayToast(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    public void switchContent(Fragment fragment, int id, boolean addToBackStack) {
/*
        mContent = fragment;
        if (addToBackStack) {
            fragment_mgr.beginTransaction()
                    .replace(R.id.content_frame, fragment, "mContent")
                    .addToBackStack(null).commit();

        } else {
            fragment_mgr.beginTransaction().replace(R.id.content_frame, fragment)
                    .commit();
        }*/
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivity = (ConnectivityManager) getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
        }
        return false;
    }

    public class LeftMenuDrawer implements DrawerLayout.DrawerListener {
        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {

        }

        @Override
        public void onDrawerOpened(View drawerView) {
        }

        @Override
        public void onDrawerClosed(View drawerView) {

        }

        @Override
        public void onDrawerStateChanged(int newState) {

        }
    }

    public void showDialog(String title, String msg, String btnText) {
        AlertDialog alertdialog = new AlertDialog.Builder(this).create();
        alertdialog.setCancelable(false);
        alertdialog.setTitle(title);
        alertdialog.setMessage(msg);
        alertdialog.setButton(DialogInterface.BUTTON_POSITIVE, btnText, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertdialog.show();
    }

    public void showLoading(String message, boolean cancelable) {

        numberOfLoadings++;
        if(processing==null) {
            processing = new MaterialDialog.Builder(this)
                    //.title(R.string.progress_dialog)
                    .content(message)
                    .progress(true, 0)
                    .backgroundColorRes(R.color.white)
                    .contentColor(getResources().getColor(R.color.black))
                    .cancelable(cancelable)
                    .show();
            processing.setOnCancelListener(new DialogInterface.OnCancelListener() {

                @Override
                public void onCancel(DialogInterface dialog) {
                }
            });
            processing.setCanceledOnTouchOutside(cancelable);
            if (!processing.isShowing()) {
                processing.show();
            }
        }
    }

    public void hideLoading() {
        if(numberOfLoadings>0)
            numberOfLoadings--;
        if (processing != null&&numberOfLoadings==0)
            processing.dismiss();
    }

    private void setupViewPager(ViewPager viewPager, ArrayList<String> mDataList) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager(), mDataList);
        viewPager.setAdapter(adapter);
        tabs.setViewPager(viewPager);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager, List<String> mFragmentTitleList) {
            super(manager);
            this.mFragmentTitleList = mFragmentTitleList;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position) {
                case 0:
                    fragment = new HomeFragment();
                    break;
                case 1:
                    fragment = new MyInvoicesFragment();
                    break;
            }
            return fragment;
        }

        @Override
        public int getItemPosition(Object object) {
            if(object.getClass().getSimpleName().equals("HomeFragment"))
                return POSITION_UNCHANGED;
            else
                return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return mFragmentTitleList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        et_search.setText("");
        numberOfLoadings=0;
        if(processing!=null)
            processing.dismiss();
        AircountrApplication.getInstance().trackScreenView(TAG);
        count =0;
    }

    @Override
    public void onBackPressed() {
        Time time = new Time();
        time.setToNow();
        if(back+3000<time.toMillis(false)){
            count = 0;
        }

        if(count==0){
            displayToast("press back again to quit");
            count=1;
            back = time.toMillis(false);
        }else{
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getSupportFragmentManager().getFragments().get(1).onActivityResult(requestCode, resultCode, data);
    }
}