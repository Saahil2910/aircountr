package com.aircountr.android.fragment;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aircountr.android.AircountrApplication;
import com.aircountr.android.BaseActivity;
import com.aircountr.android.ExpenseActivity;
import com.aircountr.android.R;
import com.aircountr.android.adapters.ExpenseAdapter;
import com.aircountr.android.constants.CreateUrl;
import com.aircountr.android.objects.ExpenseChartDataItem;
import com.aircountr.android.prefrences.AppPreferences;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by gaura on 5/21/2016.
 */
public class ExpenseFragment extends Fragment {

    private String TAG = this.getClass().getSimpleName();
    private ImageView iv_previousDateBtn;
    private TextView tv_selectedDate,tvVendorOrCategory,tv_CatTag,tv_VenTag;
    private ImageView iv_nextDateBtn;
    private PieChart mChart;
    private LinearLayout ll_pie;
    private Calendar cal;
    private SwitchCompat scVenOrCat;
    private RecyclerView rv_expenses;
    private boolean bywhich;
    private String strTimeStamp = null;
    private ArrayList<ExpenseChartDataItem> expenseChartDataList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_expense, null);
        iv_previousDateBtn = (ImageView) root.findViewById(R.id.iv_previousDateBtn);
        tv_selectedDate = (TextView) root.findViewById(R.id.tv_selectedDate);
        iv_nextDateBtn = (ImageView) root.findViewById(R.id.iv_nextDateBtn);
        mChart = (PieChart) root.findViewById(R.id.pc_expense);
        ll_pie = (LinearLayout)root.findViewById(R.id.ll_pie);
        scVenOrCat=(SwitchCompat)root.findViewById(R.id.sc_vendorOrCategory);
        tvVendorOrCategory=(TextView)root.findViewById(R.id.tv_vendorOrCategory);
        tv_CatTag=(TextView)root.findViewById(R.id.tv_CatTag);
        tv_VenTag=(TextView)root.findViewById(R.id.tv_VenTag);
        bywhich=false;
        rv_expenses=(RecyclerView)root.findViewById(R.id.rvexpenses);
        return root;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tv_selectedDate.setTypeface(((ExpenseActivity) getActivity()).SEMIBOLD);
        tvVendorOrCategory.setTypeface(((ExpenseActivity) getActivity()).REGULAR);
        tv_CatTag.setTypeface(((ExpenseActivity) getActivity()).SEMIBOLD);
        tv_VenTag.setTypeface(((ExpenseActivity) getActivity()).SEMIBOLD);
        expenseChartDataList = new ArrayList<>();
        cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        tv_selectedDate.setText(new SimpleDateFormat("MMM yyyy").format(cal.getTime()));
        strTimeStamp = String.valueOf(cal.getTimeInMillis());
        mChart.setUsePercentValues(true);
        mChart.setDescription("");
        mChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setDragDecelerationFrictionCoef(0.95f);

//        tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        mChart.setCenterTextTypeface(((ExpenseActivity) getActivity()).REGULAR);
//        mChart.setCenterText(generateCenterSpannableText());

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);

        mChart.setHoleRadius(58f);
        mChart.setTransparentCircleRadius(61f);

        mChart.setDrawCenterText(true);

        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(true);
        mChart.setHighlightPerTapEnabled(true);
        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
//        mChart.setOnChartValueSelectedListener(this);


        iv_previousDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cal.add(Calendar.MONTH, -1);
                tv_selectedDate.setText(new SimpleDateFormat("MMM yyyy").format(cal.getTime()));
                strTimeStamp = String.valueOf(cal.getTimeInMillis());
                iv_previousDateBtn.setEnabled(false);
                sendExpenseChartRequest();

            }
        });

        iv_nextDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cal.add(Calendar.MONTH, 1);
                tv_selectedDate.setText(new SimpleDateFormat("MMM yyyy").format(cal.getTime()));
                strTimeStamp = String.valueOf(cal.getTimeInMillis());
                iv_nextDateBtn.setEnabled(false);
                sendExpenseChartRequest();
            }
        });
        scVenOrCat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                bywhich = isChecked;
                if (isChecked) {
                    AircountrApplication.getInstance().trackEvent("Action", "Toggle", "To Vendor");
                    tvVendorOrCategory.setText("Total Expenses By Category");
                    tv_CatTag.setTextColor(Color.BLACK);
                    tv_VenTag.setTextColor(Color.LTGRAY);
                } else {
                    AircountrApplication.getInstance().trackEvent("Action", "Toggle", "To Category");
                    tvVendorOrCategory.setText("Total Expenses By Vendor");
                    tv_VenTag.setTextColor(Color.BLACK);
                    tv_CatTag.setTextColor(Color.LTGRAY);
                }
                sendExpenseChartRequest();
            }
        });

        sendExpenseChartRequest();
//        scVenOrCat.performClick();
    }

    private void setData() {
        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
        // IMPORTANT: In a PieChart, no values (Entry) should have the same
        // xIndex (even if from different DataSets), since no values can be
        // drawn above each other.
        Collections.sort(expenseChartDataList,new Comparator<ExpenseChartDataItem>(){

            @Override
            public int compare(ExpenseChartDataItem lhs, ExpenseChartDataItem rhs) {
                return Integer.compare(Integer.parseInt(lhs.getAmount()),Integer.parseInt(rhs.getAmount()));
            }
        });
        for (int i = 0; i < expenseChartDataList.size(); i++) {
            yVals1.add(new Entry(expenseChartDataList.get(i).getPercentage() , i));
        }
        ArrayList<String> xVals = new ArrayList<String>();

        for (int i = 0; i < expenseChartDataList.size(); i++) {
            String temp=String.format("%.4f",(expenseChartDataList.get(i).getPercentage()));
            xVals.add(temp);
        }

        mChart.getLayoutParams().height=(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,315+(Math.max(((xVals.size()) / 6), 3)-3)*15, getResources().getDisplayMetrics());
        PieDataSet dataSet = new PieDataSet(yVals1, "Expense Results");
    //    dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        mChart.getLegend().setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "ProximaNova-Regular.ttf"));
        // add a lot of colors
        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        dataSet.setSelectionShift(0f);
        Legend l = mChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        l.setWordWrapEnabled(true);
//        l.setEnabled(false);
        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter(new DecimalFormat("")));
        data.setValueTextSize(0f);
//        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
        data.setValueTypeface(((ExpenseActivity) getActivity()).REGULAR);
        mChart.setData(data);
        mChart.setBackgroundColor(Color.WHITE);
        mChart.highlightValues(null);

        mChart.invalidate();
        if(expenseChartDataList.size()==0)
            tvVendorOrCategory.setText(" ");
        for(int i=0;i<yVals1.size();i++)
        {
            expenseChartDataList.get(i).setColor(dataSet.getColor(i));
        }
        rv_expenses.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_expenses.setAdapter(new ExpenseAdapter(getActivity(),expenseChartDataList));
    }

    private void sendExpenseChartRequest() {
        if (!AppPreferences.getMerchantId(getActivity()).equals("") && !AppPreferences.getAccessToken(getActivity()).equals("")) {
            if (((ExpenseActivity) getActivity()).isNetworkAvailable()) {
                if(scVenOrCat.isChecked())
                    tvVendorOrCategory.setText("Total Expenses By Category");
                else
                    tvVendorOrCategory.setText("Total Expenses By Vendor");
                new ExpenseChartAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, strTimeStamp);
            } else {
                ((ExpenseActivity) getActivity()).showDialog("Error", getActivity().getResources().getString(R.string.no_internet), "OK");
            }
        } else {
            ((ExpenseActivity) getActivity()).showDialog("Alert", "Oh! you seems logged out", "OK");
        }
    }

    private class ExpenseChartAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ((ExpenseActivity) getActivity()).showLoading("Plz Wait...", false);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d(TAG, "Response :" + s);
            if (s != null) {
                try {
                    float toalexpenses=0;
                    expenseChartDataList.clear();
                    JSONObject response = new JSONObject(s);
                    boolean success = response.getBoolean("success");
                    JSONArray dataArray = response.getJSONArray("data");
                    if (dataArray != null && dataArray.length() > 0) {
                        for (int i = 0; i < dataArray.length(); i++) {
                            ExpenseChartDataItem rowData = new ExpenseChartDataItem();
                            JSONObject dataObj = dataArray.getJSONObject(i);
                            String _id = dataObj.getString("_id");
                            String totalInvoice = dataObj.getString("totalinvoice");
                            toalexpenses=toalexpenses+(Float.parseFloat(totalInvoice));
                            rowData.setName(_id);
                            rowData.setAmount(totalInvoice);
                            expenseChartDataList.add(rowData);
                        }
                        for(int i=0;i<dataArray.length();i++)
                        {
                            expenseChartDataList.get(i).setPercentage((Float.parseFloat(expenseChartDataList.get(i).getAmount()))*(100)/toalexpenses);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                setData();

                mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
                // mChart.spin(2000, 0, 360);
            }
            iv_previousDateBtn.setEnabled(true);
            iv_nextDateBtn.setEnabled(true);
            ((ExpenseActivity) getActivity()).hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {

            final String url;
            if(bywhich)
                url=CreateUrl.expenseCategoryChartUrl(AppPreferences.getMerchantId(getActivity()), params[0]);
            else
                url=CreateUrl.expenseVendorChartUrl(AppPreferences.getMerchantId(getActivity()), params[0]);
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Authorization", AppPreferences.getAccessToken(getActivity()));
            try {
                HttpResponse response = httpClient.execute(httpGet);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return _response;
        }
    }
}
