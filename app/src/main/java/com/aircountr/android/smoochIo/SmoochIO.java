package com.aircountr.android.smoochIo;

import android.os.Bundle;

import com.aircountr.android.BaseActivity;
import com.aircountr.android.R;

import io.smooch.ui.ConversationActivity;

/**
 * Created by Saahil on 11/06/16.
 */
public class SmoochIO extends BaseActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ConversationActivity.show(this);
        this.finish();
    }
}
