package com.aircountr.android;

import android.app.Application;
import android.text.TextUtils;

import com.aircountr.android.objects.CategoryListItem;
import com.aircountr.android.utils.LruBitmapCache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.splunk.mint.Mint;

import net.gotev.uploadservice.Logger;
import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.okhttp.OkHttpStack;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import io.smooch.core.Smooch;
import okhttp3.OkHttpClient;

/**
 * Created by architnf on 5/17/2016.
 */
public class AircountrApplication extends Application {
    public static final String TAG = AircountrApplication.class
            .getSimpleName();
    public ArrayList<CategoryListItem> mCategoryListItems;
    public String mCurrentPhotoPath;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private static AircountrApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        Mint.initAndStartSession(this, "058c75a4");
        mCategoryListItems = new ArrayList<>();
        mInstance = this;
        Smooch.init(this, "9stpiod5dket0hx6l9h1363n7");
        GoogleAnalyticsTracker.init(this);
        GoogleAnalyticsTracker.getInstance().get(GoogleAnalyticsTracker.Target.APP_TRACKER);

        // Set your application namespace to avoid conflicts with other apps
        // using this library
        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;

        // Set the HTTP stack to use. The default is HurlStack which uses HttpURLConnection.
        // To use OkHttp for example, you have to add the required dependency in your gradle file
        // and then you can simply un-comment the following line. Read the wiki for more info.
        OkHttpClient client = new OkHttpClient.Builder()
                .followRedirects(true)
                .followSslRedirects(true)
                .retryOnConnectionFailure(true)
                .connectTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .cache(null)
                .build();
        UploadService.HTTP_STACK = new OkHttpStack(client);

        // Set upload service debug log messages level
        Logger.setLogLevel(Logger.LogLevel.DEBUG);
    }

    public static synchronized AircountrApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public synchronized Tracker getGoogleAnalyticsTracker() {
        return GoogleAnalyticsTracker.getInstance().get(GoogleAnalyticsTracker.Target.APP_TRACKER);
    }

    public void trackScreenView(String screenName) {
        Tracker t = getGoogleAnalyticsTracker();
        t.setScreenName(screenName);
        t.send(new HitBuilders.ScreenViewBuilder().build());
        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();
            t.send(new HitBuilders.ExceptionBuilder().setDescription(new StandardExceptionParser(this, null).getDescription(Thread.currentThread().getName(), e))
                    .setFatal(false)
                    .build()
            );
        }
    }

    public void trackEvent(String category, String action, String label) {
        Tracker t = getGoogleAnalyticsTracker();
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
    }

    public void trackEvent(String category, String action) {
        Tracker t = getGoogleAnalyticsTracker();
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).build());
    }
}