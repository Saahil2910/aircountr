package com.aircountr.android.service;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by VikramV on 6/22/2016.
 */
public class UploadService extends IntentService {
    private int result = Activity.RESULT_CANCELED;
    private static final String NOTIFICATION = "com.aircountr.android";
    private static final String uploadUrl = "uploadUrl";
    private static final String vendorId = "vendorId";
    private static final String merchantId = "merchantId";
    private static final String categoryId = "categoryId";
    private static final String invoiceamount = "invoiceamount";
    private static final String isautomode = "isautomode";
    private static final String comments = "comments";
    private static final String createdDate = "createdDate";
    private static final String vendorname = "vendorname";
    private static final String categoryname = "categoryname";
    private static final String invoice = "invoice";

    public UploadService() {
        super("UploadDService");
    }

    // will be called asynchronously by Android
    @Override
    protected void onHandleIntent(Intent intent) {
        String urlPath = intent.getStringExtra(uploadUrl);
        String venId = intent.getStringExtra(vendorId);
        String merId = intent.getStringExtra(merchantId);
        String catId = intent.getStringExtra(categoryId);
        String invAmt = intent.getStringExtra(invoiceamount);
        Boolean isAuto = intent.getBooleanExtra(isautomode, false);
        String comnts = intent.getStringExtra(comments);

        File output = new File(Environment.getExternalStorageDirectory(),
                "Upload" + invAmt);
        if (output.exists()) {
            output.delete();
        }

        InputStream stream = null;
        FileOutputStream fos = null;
        try {
            URL url = new URL(urlPath);
            stream = url.openConnection().getInputStream();
            InputStreamReader reader = new InputStreamReader(stream);
            fos = new FileOutputStream(output.getPath());
            int next = -1;
            while ((next = reader.read()) != -1) {
                fos.write(next);
            }
            // successfully finished
            result = Activity.RESULT_OK;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        publishResults(output.getAbsolutePath(), result);
    }

    private void publishResults(String outputPath, int result) {
        Intent intent = new Intent(NOTIFICATION);
        //intent.putExtra(FILEPATH, outputPath);
        //intent.putExtra(RESULT, result);
        sendBroadcast(intent);
    }
}