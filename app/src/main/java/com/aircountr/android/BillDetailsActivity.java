package com.aircountr.android;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aircountr.android.constants.CreateUrl;
import com.aircountr.android.constants.UrlConstants;
import com.aircountr.android.prefrences.AppPreferences;
import com.aircountr.android.utils.AircountrUtils;
import com.splunk.mint.Mint;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadStatusDelegate;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by gaurav on 4/27/2016.
 */
public class BillDetailsActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener, View.OnClickListener, UrlConstants, UploadStatusDelegate {
    private String TAG = this.getClass().getSimpleName();
    private ImageView iv_backBtn;
    private TextView tv_pageTitle;
    private TextView tv_dateText;
    private TextView tv_date;
    private TextView tv_errDate;
    private TextView tv_AmountText;
    private EditText et_amount;
    private TextView tv_errAmount;
    private TextView tv_categoryText;
    private TextView tv_category;
    private TextView tv_vendorText;
    private AutoCompleteTextView actv_vendor;
    private TextView tv_errVendor;
    private TextView tv_commentText;
    private EditText et_comments;
    private TextView tv_uptoText;
    private TextView tv_addImgText;
    private ImageView iv_img1;
    private TextView tv_img1Text;
    private ImageView iv_img2;
    private TextView tv_img2Text;
    private ImageView iv_img3;
    private TextView tv_img3Text;
    private TextView tv_saveBtn;
    private ArrayList<String> mVendorDataList;
    private int imageBtnType = 0, categoryPosition;
    private final int CAMERA_REQUEST = 101, GALLERY_REQUEST = 100;
    private byte[] image_byte;
    private ByteArrayBody bab;
    private String categoryId = "", categoryName = "", vendorId = "";
    private ViewGroup vwgrp_container;
    private static final String USER_AGENT = "AirCountrInvoiceUpload/" + BuildConfig.VERSION_NAME;
    private static final int FILE_CODE = 1;
    private String filePath = "";
    private Map<String, UploadProgressViewHolder> uploadProgressHolders = new HashMap<>();
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int PERMISSION_REQUEST_READ_EXTERNAL_STORAGE = 100;
    private static final int REQUEST_PERMISSION_STORAGE_SETTING = 101;
    private static final int REQUEST_TAKE_IMAGE_FROM_GALLERY = 102;
    private static final int REQUEST_TAKE_PICTURE_FROM_CAMERA = 103;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set the application environment
        Mint.setApplicationEnvironment(Mint.appEnvironmentStaging);
        Mint.initAndStartSession(BillDetailsActivity.this, "058c75a4");

        setContentView(R.layout.activity_bill_details);

        init();
        setDefault();

        tv_dateText.setOnClickListener(this);
        tv_date.setOnClickListener(this);
        iv_img1.setOnClickListener(this);
        iv_img2.setOnClickListener(this);
        iv_img3.setOnClickListener(this);
        iv_backBtn.setOnClickListener(this);
        et_amount.addTextChangedListener(new GenericTextWatcher(et_amount));
        actv_vendor.addTextChangedListener(new GenericTextWatcher(actv_vendor));
        tv_saveBtn.setOnClickListener(this);
    }

    private void init() {
        iv_backBtn = (ImageView) findViewById(R.id.iv_backBtn);
        tv_pageTitle = (TextView) findViewById(R.id.tv_pageTitle);
        tv_dateText = (TextView) findViewById(R.id.tv_dateText);
        tv_date = (TextView) findViewById(R.id.tv_date);
        tv_errDate = (TextView) findViewById(R.id.tv_errDate);
        tv_AmountText = (TextView) findViewById(R.id.tv_AmountText);
        et_amount = (EditText) findViewById(R.id.et_amount);
        tv_errAmount = (TextView) findViewById(R.id.tv_errAmount);
        tv_categoryText = (TextView) findViewById(R.id.tv_categoryText);
        tv_category = (TextView) findViewById(R.id.tv_category);
        tv_vendorText = (TextView) findViewById(R.id.tv_vendorText);
        actv_vendor = (AutoCompleteTextView) findViewById(R.id.actv_vendor);
        tv_errVendor = (TextView) findViewById(R.id.tv_errVendor);
        tv_commentText = (TextView) findViewById(R.id.tv_commentText);
        et_comments = (EditText) findViewById(R.id.et_comments);
        tv_uptoText = (TextView) findViewById(R.id.tv_uptoText);
        tv_addImgText = (TextView) findViewById(R.id.tv_addImgText);
        iv_img1 = (ImageView) findViewById(R.id.iv_img1);
        tv_img1Text = (TextView) findViewById(R.id.tv_img1Text);
        iv_img2 = (ImageView) findViewById(R.id.iv_img2);
        tv_img2Text = (TextView) findViewById(R.id.tv_img2Text);
        iv_img3 = (ImageView) findViewById(R.id.iv_img3);
        tv_img3Text = (TextView) findViewById(R.id.tv_img3Text);
        tv_saveBtn = (TextView) findViewById(R.id.tv_saveBtn);
        vwgrp_container = (ViewGroup) findViewById(R.id.container);

        tv_pageTitle.setTypeface(SEMIBOLD);
        tv_dateText.setTypeface(REGULAR);
        tv_date.setTypeface(REGULAR);
        tv_errDate.setTypeface(REGULAR);
        tv_AmountText.setTypeface(REGULAR);
        et_amount.setTypeface(REGULAR);
        tv_errAmount.setTypeface(REGULAR);
        tv_categoryText.setTypeface(REGULAR);
        tv_category.setTypeface(REGULAR);
        tv_vendorText.setTypeface(REGULAR);
        actv_vendor.setTypeface(REGULAR);
        tv_errVendor.setTypeface(REGULAR);
        tv_commentText.setTypeface(REGULAR);
        et_comments.setTypeface(REGULAR);
        tv_uptoText.setTypeface(REGULAR);
        tv_addImgText.setTypeface(REGULAR);
        tv_img1Text.setTypeface(REGULAR);
        tv_img2Text.setTypeface(REGULAR);
        tv_img3Text.setTypeface(REGULAR);
        tv_saveBtn.setTypeface(REGULAR);

        actv_vendor.setThreshold(1);
    }

    private void setDefault() {
        Calendar now = Calendar.getInstance();
        tv_date.setText(now.get(Calendar.DAY_OF_MONTH) + "/" + (now.get(Calendar.MONTH) + 1) + "/" + now.get(Calendar.YEAR));

        Intent intent = getIntent();
        categoryId = intent.getStringExtra(CATEGORY_ID);
        categoryName = intent.getStringExtra(CATEGORY_NAME);
        categoryPosition = intent.getIntExtra(CATEGORY_POSITION, 0);
        this.mVendorDataList = ((AircountrApplication) getApplicationContext().getApplicationContext()).mCategoryListItems.get(categoryPosition).getAllVendorsNames();

        tv_category.setText(categoryName);
        ArrayAdapter<String> vendorTextAdapter = new ArrayAdapter<String>(BillDetailsActivity.this, R.layout.row_gps_address_list, mVendorDataList);
        actv_vendor.setAdapter(vendorTextAdapter);
        if (mVendorDataList != null && mVendorDataList.size() > 0)
            actv_vendor.setText(mVendorDataList.get(0));
    }

    public void datePickerDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                BillDetailsActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setThemeDark(false);
        dpd.vibrate(false);
        dpd.dismissOnPause(true);
        dpd.showYearPickerFirst(false);
        dpd.setAccentColor(getResources().getColor(R.color.theme_color_blue));
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = dayOfMonth + "/" + (++monthOfYear) + "/" + year;
        tv_date.setText(date);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.tv_date:
                datePickerDialog();
                break;
            case R.id.tv_dateText:
                datePickerDialog();
                break;
            case R.id.iv_img1:
                imageBtnType = 1;
                selectImgOptionPopUp();
                break;
            case R.id.iv_img2:
//                imageBtnType = 2;
//                selectImgOptionPopUp();
                break;
            case R.id.iv_img3:
//                imageBtnType = 3;
//                selectImgOptionPopUp();
                break;
            case R.id.tv_saveBtn:
                if (checkValidation()) {
                    showLoading("Plz wait...", false);
                    tv_saveBtn.setClickable(false);
                    final String fileUploadUrl = CreateUrl.uploadBillDetail();
                    //String postBody = String.format("vendorId:\nmerchantId:" + AppPreferences.getMerchantId(BillDetailsActivity.this) + "\ncategoryId");
                    //String filePathFinal = "";
                    //try {
                    //filePathFinal = AircountrUtils.writeToFileAndGetPath(BillDetailsActivity.this, postBody);
                    // } catch (IOException e) {
                    //  e.printStackTrace();
                    //}
                    if (!filePath.equalsIgnoreCase("")) {
                        try {
                            final String fileName = AircountrUtils.getFileName(filePath);
                            MultipartUploadRequest req = new MultipartUploadRequest(this, fileUploadUrl)
                                    .addFileToUpload(filePath, "invoice")
                                    .addHeader("Authorization", AppPreferences.getAccessToken(BillDetailsActivity.this))
                                    .addParameter("merchantId", AppPreferences.getMerchantId(BillDetailsActivity.this))
                                    .addParameter("categoryId", categoryId)
                                    .addParameter("invoiceamount", et_amount.getText().toString().trim())
                                    .addParameter("isautomode", "false")
                                    .addParameter("comments", et_comments.getText().toString().trim())
                                    .addParameter("createdDate", tv_date.getText().toString().trim())
                                    .addParameter("categoryname", categoryName)
                                    .addParameter("vendorname", actv_vendor.getText().toString().trim())
                                    .addParameter("isprocessed", "true")
                        /*if (bab != null)
                            entity.addPart("invoice", bab);*/
                                            //.setNotificationConfig(getNotificationConfig(fileName))
//                                .setCustomUserAgent(USER_AGENT)
//                                .setAutoDeleteFilesAfterSuccessfulUpload(false)
//                                .setUsesFixedLengthStreamingMode(true)
                                    .setMaxRetries(3);

                            if (!vendorId.equals("")) {
                                req.addParameter("vendorId", vendorId);
                            }

                            String uploadID = req.setDelegate(this).startUpload();
/*                        final String uploadID = new BinaryUploadRequest(this, fileUploadUrl)
                                //.addHeader("file-name", new File(fileToUploadPath).getName())
                                .addHeader("Authorization", AppPreferences.getAccessToken(BillDetailsActivity.this))
                                .addHeader("Content-Type", "multipart/form-data")
                                *//*.addParameter("vendorId", "")
                                .addParameter("merchantId", AppPreferences.getMerchantId(BillDetailsActivity.this))
                                .addParameter("categoryId", categoryId)
                                .addParameter("invoiceamount", et_amount.getText().toString().trim())
                                .addParameter("isautomode", "false")
                                .addParameter("comments", et_comments.getText().toString().trim())
                                .addParameter("createdDate", tv_date.getText().toString().trim())
                                .addParameter("categoryname", actv_vendor.getText().toString().trim())
                                .addParameter("isprocessed", "true")*//*
                                //.setFileToUpload(filePath)
                                .setFileToUpload(filePathFinal)
                                .setNotificationConfig(getNotificationConfig(fileName))
                                .setCustomUserAgent(USER_AGENT)
                                .setAutoDeleteFilesAfterSuccessfulUpload(false)
                                .setUsesFixedLengthStreamingMode(true)
                                .setMaxRetries(2)
                                .setDelegate(this)
                                .startUpload();*/
                            //addUploadToList(uploadID, fileName);
                            // these are the different exceptions that may be thrown
                            tv_saveBtn.setClickable(true);
                            hideLoading();
                            switchActivity(this, HomeActivity.class);
                        } catch (FileNotFoundException exc) {
                            showToast(exc.getMessage());
                            tv_saveBtn.setClickable(true);
                        } catch (IllegalArgumentException exc) {
                            showToast("Missing some arguments. " + exc.getMessage());
                            tv_saveBtn.setClickable(true);
                        } catch (MalformedURLException exc) {
                            showToast(exc.getMessage());
                            tv_saveBtn.setClickable(true);
                        }
                    }else{
                            sendBillDetailRequest(categoryId, et_amount.getText().toString().trim(), et_comments.getText().toString().trim(), tv_date.getText().toString().trim(), actv_vendor.getText().toString().trim());
                        }
                    }
                    break;
                    case R.id.iv_backBtn:
                        BillDetailsActivity.this.finish();
                        break;
        }
    }

    private boolean checkValidation() {
        if (tv_date.getText().toString().equals("")) {
            tv_errDate.setVisibility(View.VISIBLE);
            tv_errDate.setText("Plz select date");
            return false;
        } else if (et_amount.getText().toString().trim().isEmpty()) {
            tv_errAmount.setVisibility(View.VISIBLE);
            tv_errAmount.setText("Plz enter amount");
            return false;
        } else if (actv_vendor.getText().toString().trim().isEmpty()) {
            tv_errVendor.setVisibility(View.VISIBLE);
            tv_errVendor.setText("Plz enter vendor name");
            return false;
        } else {
            vendorId = "";
            if (mVendorDataList != null && mVendorDataList.size() > 0) {
                String vendorname = actv_vendor.getText().toString().trim();
                for (int i = 0; i < mVendorDataList.size(); i++) {
                    if (mVendorDataList.get(i).equals(vendorname)) {
                        vendorId = ((AircountrApplication) getApplicationContext().getApplicationContext()).mCategoryListItems.get(categoryPosition).getVendorsDataList().get(i).getVendorId();
                        break;
                    }
                }
            }
            tv_errDate.setVisibility(View.GONE);
            tv_errAmount.setVisibility(View.GONE);
            tv_errVendor.setVisibility(View.GONE);
            return true;
        }
    }

    private void selectImgOptionPopUp() {
        final Dialog dialog = new Dialog(BillDetailsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.pop_up_select_img_layout);
        TextView tv_cameraBtn = (TextView) dialog.findViewById(R.id.tv_cameraBtn);
        TextView tv_galleryBtn = (TextView) dialog.findViewById(R.id.tv_galleryBtn);
        TextView tv_cancelBtn = (TextView) dialog.findViewById(R.id.tv_cancelBtn);

        tv_cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //startActivityForResult(intent, CAMERA_REQUEST);
                File file = new File(BillDetailsActivity.this.getExternalFilesDir(Environment.DIRECTORY_PICTURES).getAbsolutePath(), "IMG" + System.currentTimeMillis() + ".jpg");
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    Uri outputUri = null;
                    filePath = file.getAbsolutePath();
                    outputUri = Uri.fromFile(file);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputUri);
                }
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PICTURE_FROM_CAMERA);
                dialog.dismiss();
            }
        });

        tv_galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkIfStorageAccessPermissionsGranted()) {
                    takeImageFromGallery();
                }
//                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//                photoPickerIntent.setType("image/*");
//                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
                dialog.dismiss();
            }
        });

        tv_cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void sendBillDetailRequest(String categoryId, String amount, String comment, String date, String vendorName) {
        if (!AppPreferences.getMerchantId(BillDetailsActivity.this).equals("") && !AppPreferences.getAccessToken(BillDetailsActivity.this).equals("")) {
            if (isNetworkAvailable()) {
                new BillDetailAsync().execute(categoryId, amount, comment, date, vendorName);
            } else {
                showDialog("Error", getResources().getString(R.string.no_internet), "OK");
            }
        } else {
            switchActivity(BillDetailsActivity.this, SignInActivity.class);
        }
    }

    private class BillDetailAsync extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading("Plz wait...", false);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                try {
                    JSONObject response = new JSONObject(result);
                    boolean success = response.getBoolean("success");
                    String msg = response.getString("msg");
                    if (success) {
                        displayToast(msg);
                        HomeActivity.isInvoiceListChanged = true;
                        BillDetailsActivity.this.finish();
                    } else {
                        showDialog("Alert", msg, "OK");
                    }

                } catch (JSONException e) {
                    AircountrApplication.getInstance().trackException(e);
                    e.printStackTrace();
                    hideLoading();
                }
            }
            hideLoading();
        }

        @Override
        protected String doInBackground(String... params) {
            final String url = CreateUrl.uploadBillDetail();
            String _response = null;

            HttpClient httpClient = new DefaultHttpClient();
            HttpConnectionParams.setConnectionTimeout(httpClient.getParams(), 30000);
            HttpPost httpPost = new HttpPost(url);
            httpPost.setHeader("Authorization", AppPreferences.getAccessToken(BillDetailsActivity.this));
            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            try {
                entity.addPart("vendorId", new StringBody(vendorId));
                entity.addPart("merchantId", new StringBody(AppPreferences.getMerchantId(BillDetailsActivity.this)));
                entity.addPart("categoryId", new StringBody(params[0]));
                entity.addPart("invoiceamount", new StringBody(params[1]));
                entity.addPart("isautomode", new StringBody("false"));
                entity.addPart("comments", new StringBody(params[2]));
                entity.addPart("createdDate", new StringBody(params[3]));
                entity.addPart("vendorname", new StringBody(params[4]));
                entity.addPart("categoryname", new StringBody(categoryName));
                entity.addPart("isprocessed", new StringBody("true"));
               /* if (bab != null)
                    entity.addPart("invoice", bab);*/

                httpPost.setEntity(entity);
                HttpResponse response = httpClient.execute(httpPost);
                _response = EntityUtils.toString(response.getEntity());
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                AircountrApplication.getInstance().trackException(e);
                e.printStackTrace();
            }
            Log.d(TAG, "Response : " + _response);
            return _response;
        }
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        switch (requestCode) {
//            case GALLERY_REQUEST:
//                if (resultCode == RESULT_OK) {
//                    if (data != null) {
//                        Uri selectedImage = data.getData();
//                        verifyStoragePermissions(BillDetailsActivity.this);
//                        File finalFile = new File(getRealPathFromURI(selectedImage));
//                        filePath = finalFile.getAbsolutePath();
//                        iv_img1.setImageURI(selectedImage);
//                        try {
//                            FileInputStream imageStream = (FileInputStream) getContentResolver().openInputStream(selectedImage);
//                            Bitmap selectedImg = BitmapFactory.decodeStream(imageStream);
//                            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//                            selectedImg.compress(Bitmap.CompressFormat.JPEG, 70, outputStream);
//                            image_byte = outputStream.toByteArray();
//                            bab = new ByteArrayBody(image_byte, System.currentTimeMillis() + ".jpg");
//                        } catch (FileNotFoundException e) {
//                            AircountrApplication.getInstance().trackException(e);
//                            e.printStackTrace();
//                        }
//                    } else {
//                        displayToast("Selected image not found");
//                    }
//                }
//                break;
//            case CAMERA_REQUEST:
//                if (resultCode == RESULT_OK) {
//                    Bitmap photo = (Bitmap) data.getExtras().get("data");
//                    iv_img1.setImageBitmap(photo);
//                    Uri tempUri = getImageUri(getApplicationContext(), photo);
//                    File finalFile = new File(getRealPathFromURI(tempUri));
//                    filePath = finalFile.getAbsolutePath();
//                    Log.d(TAG, "A PATH : " + finalFile.getAbsolutePath());
//                    Log.d(TAG, "B PATH : " + finalFile.getPath());
//                    verifyStoragePermissions(BillDetailsActivity.this);
//                    Bitmap selectedImg = BitmapFactory.decodeFile(finalFile.getAbsolutePath());
//                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//                    selectedImg.compress(Bitmap.CompressFormat.PNG, 70, outputStream);
//                    image_byte = outputStream.toByteArray();
//                    bab = new ByteArrayBody(image_byte, System.currentTimeMillis() + ".png");
//                }
//                break;
//        }
//    }

    @Override
    protected void onResume() {
        super.onResume();
        DatePickerDialog dpd = (DatePickerDialog) getFragmentManager().findFragmentByTag("Datepickerdialog");
        if (dpd != null) dpd.setOnDateSetListener(this);

        if (tv_date.getText().toString().equals("")) {
            tv_errDate.setVisibility(View.VISIBLE);
            tv_errDate.setText("Plz select date");
        } else {
            tv_errDate.setVisibility(View.GONE);
        }
        AircountrApplication.getInstance().trackScreenView(TAG);
    }

    private class GenericTextWatcher implements TextWatcher {

        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String text = s.toString();
            switch (view.getId()) {
                case R.id.et_amount:
                    if (text.length() > 0)
                        tv_errAmount.setVisibility(View.GONE);
                    else {
                        tv_errAmount.setVisibility(View.VISIBLE);
                        tv_errAmount.setText("Plz enter amount");
                    }
                    if (tv_date.getText().toString().trim().isEmpty()) {
                        tv_errDate.setVisibility(View.VISIBLE);
                        tv_errDate.setText("Plz select date");
                    } else {
                        tv_errDate.setVisibility(View.GONE);
                    }
                    break;
                case R.id.actv_vendor:
                    if (text.length() > 0)
                        tv_errVendor.setVisibility(View.GONE);
                    else {
                        tv_errVendor.setVisibility(View.VISIBLE);
                        tv_errVendor.setText("Plz enter vendor name");
                    }
                    if (tv_date.getText().toString().trim().isEmpty()) {
                        tv_errDate.setVisibility(View.VISIBLE);
                        tv_errDate.setText("Plz select date");
                    } else {
                        tv_errDate.setVisibility(View.GONE);
                    }
                    break;
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private UploadNotificationConfig getNotificationConfig(String filename) {
//        if (!displayNotification.isChecked()) return null;

        return new UploadNotificationConfig()
                .setIcon(R.drawable.ic_upload)
                .setTitle(filename)
                .setInProgressMessage(getString(R.string.uploading))
                .setCompletedMessage(getString(R.string.upload_success))
                .setErrorMessage(getString(R.string.upload_error))
                .setAutoClearOnSuccess(true)
                .setClickIntent(new Intent(this, BillDetailsActivity.class))
                .setClearOnAction(true)
                .setRingToneEnabled(true);
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void addUploadToList(String uploadID, String filename) {
        View uploadProgressView = getLayoutInflater().inflate(R.layout.view_upload_progress, null);
        UploadProgressViewHolder viewHolder = new UploadProgressViewHolder(uploadProgressView, filename);
        viewHolder.uploadId = uploadID;
        vwgrp_container.addView(viewHolder.itemView, 0);
        uploadProgressHolders.put(uploadID, viewHolder);
    }

    class UploadProgressViewHolder {
        View itemView;
        TextView uploadTitle;
        ProgressBar progressBar;
        String uploadId;

        UploadProgressViewHolder(View view, String filename) {
            itemView = view;
            uploadTitle = (TextView) itemView.findViewById(R.id.uploadTitle);
            progressBar = (ProgressBar) itemView.findViewById(R.id.uploadProgress);

            progressBar.setMax(100);
            progressBar.setProgress(0);

            uploadTitle.setText(getString(R.string.upload_progress, filename));
        }

    }

    @Override
    public void onProgress(UploadInfo uploadInfo) {
        Log.i(TAG, String.format(Locale.getDefault(), "ID: %1$s (%2$d%%) at %3$.2f Kbit/s",
                uploadInfo.getUploadId(), uploadInfo.getProgressPercent(),
                uploadInfo.getUploadRate()));
//        logSuccessfullyUploadedFiles(uploadInfo.getSuccessfullyUploadedFiles());
//
//        if (uploadProgressHolders.get(uploadInfo.getUploadId()) == null)
//            return;
//
//        uploadProgressHolders.get(uploadInfo.getUploadId())
//                .progressBar.setProgress(uploadInfo.getProgressPercent());
    }

    @Override
    public void onError(UploadInfo uploadInfo, Exception exception) {
        Log.e(TAG, "Error with ID: " + uploadInfo.getUploadId() + ": "
                + exception.getLocalizedMessage(), exception);
//        logSuccessfullyUploadedFiles(uploadInfo.getSuccessfullyUploadedFiles());
//
//        if (uploadProgressHolders.get(uploadInfo.getUploadId()) == null)
//            return;
//
//        vwgrp_container.removeView(uploadProgressHolders.get(uploadInfo.getUploadId()).itemView);
//        uploadProgressHolders.remove(uploadInfo.getUploadId());
    }

    @Override
    public void onCompleted(UploadInfo uploadInfo, ServerResponse serverResponse) {
        Log.i(TAG, String.format(Locale.getDefault(),
                "ID %1$s: completed in %2$ds at %3$.2f Kbit/s. Response code: %4$d, body:[%5$s]",
                uploadInfo.getUploadId(), uploadInfo.getElapsedTime() / 1000,
                uploadInfo.getUploadRate(), serverResponse.getHttpCode(),
                serverResponse.getBodyAsString()));
        //logSuccessfullyUploadedFiles(uploadInfo.getSuccessfullyUploadedFiles());
        for (Map.Entry<String, String> header : serverResponse.getHeaders().entrySet()) {
            Log.i("Header", header.getKey() + ": " + header.getValue());
        }

        if (serverResponse.getHttpCode() == 200) {
            try {
                JSONObject responseObject = new JSONObject(serverResponse.getBodyAsString());
                boolean isSuccess = responseObject.getBoolean("success");
                String message = responseObject.getString("msg");
                Toast.makeText(BillDetailsActivity.this, message, Toast.LENGTH_SHORT).show();
                if (isSuccess) {
                    HomeActivity.isInvoiceListChanged = true;
                    HomeActivity.checkForUpdate(1);
                }
                BillDetailsActivity.this.finish();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(BillDetailsActivity.this, "Error while uploading the invoice.", Toast.LENGTH_SHORT).show();
        }

//        if (uploadProgressHolders.get(uploadInfo.getUploadId()) == null)
//            return;
//
//        vwgrp_container.removeView(uploadProgressHolders.get(uploadInfo.getUploadId()).itemView);
//        uploadProgressHolders.remove(uploadInfo.getUploadId());
    }

    @Override
    public void onCancelled(UploadInfo uploadInfo) {
        Log.i(TAG, "Upload with ID " + uploadInfo.getUploadId() + " is cancelled");
        //logSuccessfullyUploadedFiles(uploadInfo.getSuccessfullyUploadedFiles());

//        if (uploadProgressHolders.get(uploadInfo.getUploadId()) == null)
//            return;
//
//        vwgrp_container.removeView(uploadProgressHolders.get(uploadInfo.getUploadId()).itemView);
//        uploadProgressHolders.remove(uploadInfo.getUploadId());
    }

    private void logSuccessfullyUploadedFiles(List<String> files) {
        for (String file : files) {
            Log.e(TAG, "Success:" + file);
        }
    }

    /**
     * Checks if the app has permission to write to device storage
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    private void takeImageFromGallery() {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= 19) {
            // For Android versions of KitKat or later, we use a
            // different intent to ensure
            // we can get the file path from the returned intent URI
            intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        } else {
            intent.setAction(Intent.ACTION_GET_CONTENT);
        }

        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_TAKE_IMAGE_FROM_GALLERY);
    }

    /**
     * Check if external storage read permission have been granted.
     *
     * @return {@code true} if permissions grated, {@code false} otherwise
     */
    private boolean checkIfStorageAccessPermissionsGranted() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M)
            return true;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return true;
        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            new AlertDialog.Builder(this)
                    .setTitle("Storage access")
                    .setMessage("Storage access required to take image from the gallery")
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestStoragePermission();
                        }
                    }).create().show();
        } else {
            // No explanation needed, we can request the permission.
            requestStoragePermission();
        }
        return false;
    }

    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                PERMISSION_REQUEST_READ_EXTERNAL_STORAGE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_READ_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted,
                    takeImageFromGallery();
                } else {
                    // permission denied!
                    // next time use opens the app show him the empty screen with message to enable the location settings
                    new AlertDialog.Builder(this)
                            .setMessage("Please enable storage access.")
                            .setPositiveButton("Go to settings", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivityForResult(intent, REQUEST_PERMISSION_STORAGE_SETTING);
                                }
                            })
                            .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            }).create().show();
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_TAKE_IMAGE_FROM_GALLERY) {
                Uri uri = data.getData();
                try {
                    String path = getPath(uri);
                    setPictureAndShowInImageView(path);
                } catch (URISyntaxException e) {
                    Toast.makeText(this,
                            "Unable to get the file from the given URI.  See error log for details",
                            Toast.LENGTH_LONG).show();
                    Log.e(TAG, "Unable to upload file from the given uri", e);
                }
            } else if (requestCode == REQUEST_PERMISSION_STORAGE_SETTING) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        PERMISSION_REQUEST_READ_EXTERNAL_STORAGE);
            } else if (requestCode == REQUEST_TAKE_PICTURE_FROM_CAMERA) {
                // check if the file to which the picture from camera is saved does exists
                if (!TextUtils.isEmpty(filePath)) {
                    File file = new File(filePath);
                    setPictureAndShowInImageView(filePath);
                    Log.d(TAG, "Picture from camera saved at : " + file.getAbsolutePath());
                }
            }
        }
    }

    private void setPictureAndShowInImageView(String path) {
        filePath = path;
        // set the image into your imageview
        iv_img1.setImageURI(Uri.fromFile(new File(path)));
    }

    /*
     * Gets the file path of the given Uri.
     */
    @SuppressLint("NewApi")
    private String getPath(Uri uri) throws URISyntaxException {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
}