package com.aircountr.android.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.aircountr.android.AircountrApplication;
import com.aircountr.android.R;
import com.aircountr.android.objects.CategoryListItem;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;

/**
 * Created by gaurav on 5/12/2016.
 */
public class CategorySpinnerAdapter extends BaseAdapter {

    private String TAG = this.getClass().getSimpleName();
    private ArrayList<CategoryListItem> mDataList;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    public Typeface REGULAR;
    ImageLoader imageLoader = AircountrApplication.getInstance().getImageLoader();

    public CategorySpinnerAdapter(Context mContext, ArrayList<CategoryListItem> mDataList) {
        this.mContext = mContext;
        this.mDataList = mDataList;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        REGULAR = Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Regular.ttf");
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mLayoutInflater.inflate(R.layout.row_category_spinner, null);
        NetworkImageView itemIcon = (NetworkImageView) convertView.findViewById(R.id.iv_itemIcon);
        TextView itemName = (TextView) convertView.findViewById(R.id.tv_itemName);
        itemName.setTypeface(REGULAR);

        itemIcon.setImageUrl(mDataList.get(position).getResourceId(), imageLoader);
        itemName.setText(mDataList.get(position).getCategoryName());
        return convertView;
    }
}
