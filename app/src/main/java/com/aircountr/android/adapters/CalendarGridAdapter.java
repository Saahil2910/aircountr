package com.aircountr.android.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aircountr.android.R;
import com.aircountr.android.objects.CalendarListItem;

import java.util.ArrayList;

/**
 * Created by gaura on 5/19/2016.
 */
public class CalendarGridAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<CalendarListItem> mCalendarDataList;
    private LayoutInflater mLayoutInflater;
    private Typeface REGULAR;

    public CalendarGridAdapter(Context mContext, ArrayList<CalendarListItem> mCalendarDataList) {
        this.mContext = mContext;
        this.mCalendarDataList = mCalendarDataList;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        REGULAR = Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Regular.ttf");
    }

    @Override
    public int getCount() {
        return mCalendarDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return mCalendarDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void onDataSetChanged(ArrayList<CalendarListItem> mCalendarDataList) {
        this.mCalendarDataList = mCalendarDataList;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mLayoutInflater.inflate(R.layout.column_calendar_grid, null);
        TextView tv_date = (TextView) convertView.findViewById(R.id.tv_date);
        TextView tv_amount = (TextView) convertView.findViewById(R.id.tv_amount);
        ImageView iv_comment = (ImageView) convertView.findViewById(R.id.iv_comment);
        LinearLayout ll_amount = (LinearLayout) convertView.findViewById(R.id.ll_amount);
        tv_date.setTypeface(REGULAR);
        tv_amount.setTypeface(REGULAR);

        tv_date.setText("" + mCalendarDataList.get(position).getDate());
        if (mCalendarDataList.get(position).getAmount().equals("0")) {
            ll_amount.setVisibility(View.GONE);
        } else {
            ll_amount.setVisibility(View.VISIBLE);
            tv_amount.setText("" + mCalendarDataList.get(position).getAmount());
        }

        if (mCalendarDataList.get(position).getComments() != null && mCalendarDataList.get(position).getComments().size() > 0) {
            iv_comment.setVisibility(View.VISIBLE);
        } else {
            iv_comment.setVisibility(View.INVISIBLE);
        }

        if (mCalendarDataList.get(position).getDate() == 0) {
            convertView.setVisibility(View.INVISIBLE);
        } else {
            convertView.setVisibility(View.VISIBLE);
        }

        return convertView;
    }
}