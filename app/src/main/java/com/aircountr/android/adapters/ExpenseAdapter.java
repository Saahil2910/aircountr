package com.aircountr.android.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aircountr.android.R;
import com.aircountr.android.objects.ExpenseChartDataItem;

import java.util.ArrayList;

/**
 * Created by VOJJALA TEJA on 13-06-2016.
 */
public class ExpenseAdapter extends RecyclerView.Adapter<ExpenseAdapter.ExpenseViewHolder>{

    ArrayList<ExpenseChartDataItem> expenseChartDataList;
    Context mContext;
    public ExpenseAdapter( Context mContext,ArrayList<ExpenseChartDataItem> expenseChartDataList){
        this.expenseChartDataList=expenseChartDataList;
        this.mContext=mContext;
    }
    public class ExpenseViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView Name;
        TextView amount;
        TextView bookmark;
        ExpenseViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv_exp);
            Name = (TextView)itemView.findViewById(R.id.vendor_name);
            amount=(TextView)itemView.findViewById(R.id.tv_totalexpenses);
            bookmark=(TextView)itemView.findViewById(R.id.tv_bookmark);
            Name.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Regular.ttf"));
            amount.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "ProximaNova-Regular.ttf"));
            bookmark.setTypeface(Typeface.createFromAsset(mContext.getAssets(), "fontawesome-webfont.ttf"));
        }
    }

    @Override
    public ExpenseViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.expenses_cardview, viewGroup, false);
        ExpenseViewHolder evh = new ExpenseViewHolder(v);
        return evh;
    }

    @Override
    public void onBindViewHolder(ExpenseViewHolder expensesViewHolder, int i) {
        expensesViewHolder.Name.setText(expenseChartDataList.get(i).getName());
        expensesViewHolder.amount.setText(mContext.getResources().getString(R.string.txt_rupee)+expenseChartDataList.get(i).getAmount());
        expensesViewHolder.cv.setCardBackgroundColor(Color.WHITE);
        expensesViewHolder.bookmark.setTextColor(expenseChartDataList.get(i).getColor());
    }

    @Override
    public int getItemCount() {
        return expenseChartDataList.size();
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
